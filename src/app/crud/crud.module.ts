import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrudRoutingModule } from './crud-routing.module';
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {OrdersService} from "./shared/services/orders.service";
import {ReactiveFormsModule} from "@angular/forms";
import { CrudComponent } from './crud/crud.component';
import {OrdersComponent} from "./shared/components/orders/orders.component";
import {OrderListComponent} from "./shared/components/order-list/order-list.component";
import {SpinnerComponent} from "../shared/components/spinner/spinner.component";
import {TranslocoModule} from "@ngneat/transloco";


@NgModule({
  declarations: [CrudComponent, OrdersComponent, OrderListComponent, SpinnerComponent],
  imports: [
    CommonModule,
    CrudRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule,
    TranslocoModule
  ],
  providers: [OrdersService]
})
export class CrudModule { }
