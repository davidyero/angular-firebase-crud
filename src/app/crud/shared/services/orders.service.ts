import {Injectable} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  coffeeOrders = 'coffeeOrders';

  constructor(private firestore: AngularFirestore) {
  }

  form = new FormGroup({
    customerName: new FormControl(''),
    orderNumber: new FormControl(''),
    coffeeOrder: new FormControl(''),
    completed: new FormControl(false)
  });

  createCoffeeOrder(data) {
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection(this.coffeeOrders)
        .add(data)
        .then(
          () => {
          },
          err => {
            reject(err)
          }
        );
    });
  }

  getCoffeeOrders() {
    return this.firestore.collection(this.coffeeOrders).snapshotChanges();
  }

  updateCoffeeOrder(data) {
    return this.firestore
      .collection(this.coffeeOrders)
      .doc(data.payload.doc.id)
      .set({completed: true}, {merge: true});
  }

  deleteCoffeeOrder(data) {
    return this.firestore
      .collection(this.coffeeOrders)
      .doc(data.payload.doc.id)
      .delete();
  }
}
