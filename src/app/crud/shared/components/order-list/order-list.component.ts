import {Component, OnInit} from '@angular/core';
import {OrdersService} from "../../services/orders.service";

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['../../../../shared/styles/styles.scss']
})
export class OrderListComponent implements OnInit {

  isLoading = true;
  coffeeOrders;

  constructor(private ordersService: OrdersService) {
  }

  ngOnInit() {
    this.getCoffeeOrders();
  }

  getCoffeeOrders() {
    this.ordersService
      .getCoffeeOrders()
      .subscribe(res => {
        this.coffeeOrders = res;
        this.isLoading = false;
      });
  }

  markCompleted = data => this.ordersService.updateCoffeeOrder(data);

  deleteOrder = data => this.ordersService.deleteCoffeeOrder(data);
}
