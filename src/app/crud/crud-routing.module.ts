import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrudComponent} from "./crud/crud.component";
import {SharedConstants} from "../shared/constants/shared-constants";


const routes: Routes = [
  {
    path: SharedConstants.ROUTES.CRUD,
    component: CrudComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudRoutingModule { }
