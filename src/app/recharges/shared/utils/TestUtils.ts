import {TypeRechargeModel} from "../models/type-recharge.model";

export class TestUtils {
  static RECHARGES_MODEL: TypeRechargeModel[] = [
    {
      name: 'PlayStation',
      code: '1'
    },
    {
      name: 'Xbox',
      code: '2'
    },
    {
      name: 'Nintendo',
      code: '3'
    }
  ];
}
