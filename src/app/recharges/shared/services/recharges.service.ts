import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {TypeRechargeModel} from "../models/type-recharge.model";
import {TestUtils} from "../utils/TestUtils";

@Injectable({
  providedIn: 'root'
})
export class RechargesService {

  constructor() { }

  public getTypeRecharges(): Observable<TypeRechargeModel[]> {
    return of(TestUtils.RECHARGES_MODEL);
  }
}
