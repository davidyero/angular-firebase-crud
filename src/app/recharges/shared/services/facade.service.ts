import { Injectable } from '@angular/core';
import {RechargesStateModel} from "../models/recharges-state.model";
import {TypeRechargeModel} from "../models/type-recharge.model";
import {Observable} from "rxjs";
import {RechargesService} from "./recharges.service";

@Injectable({
  providedIn: 'root'
})
export class FacadeService {

  private state: RechargesStateModel;

  constructor(private rechargesService: RechargesService) {
  }

  public getRechargesState(): RechargesStateModel {
    return this.state;
  }

  public setRechargesState(state: RechargesStateModel) {
    this.state = state;
  }

  public getTypeRecharges(): Observable<TypeRechargeModel[]> {
    return this.rechargesService.getTypeRecharges();
  }
}
