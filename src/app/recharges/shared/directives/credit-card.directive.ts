import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[creditCard]'
})
export class CreditCardDirective {

  private MAX_LENGTH_CREDIT_CARD_NUMBER = 16;
  private GROUP_CARD_NUMBER = 4;

  @HostListener('input', ['$event.target'])
  onKeyDown(input: HTMLInputElement): void {
    let trimmed = input.value.replace(/\s+/g, '').replace(/[^0-9 ]/g, '');

    if (trimmed.length > this.MAX_LENGTH_CREDIT_CARD_NUMBER) {
      trimmed = trimmed.substr(0, this.MAX_LENGTH_CREDIT_CARD_NUMBER);
    }

    const numbers = [];
    for (let i = 0; i < trimmed.length; i += this.GROUP_CARD_NUMBER) {
      numbers.push(trimmed.substr(i, this.GROUP_CARD_NUMBER));
    }

    input.value = numbers.join(' ');
  }
}
