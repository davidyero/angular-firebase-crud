import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {

  @Input() currentStep: number;
  @Input() totalSteps: number;
  @Input() nameStep: string;

  public actualPercentage;

  constructor() {}

  ngOnInit() {
    this.actualPercentage = (this.currentStep*100)/this.totalSteps;
    (document.querySelector('.progress-bar') as HTMLElement).style.width = `${Math.round(this.actualPercentage)}%`;
  }

}
