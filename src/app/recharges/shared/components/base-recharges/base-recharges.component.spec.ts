import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseRechargesComponent } from './base-recharges.component';

describe('BaseRechargesComponent', () => {
  let component: BaseRechargesComponent;
  let fixture: ComponentFixture<BaseRechargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseRechargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseRechargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
