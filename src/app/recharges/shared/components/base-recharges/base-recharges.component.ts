import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {RechargesStateModel} from "../../models/recharges-state.model";
import {NavigationModel} from "../../models/navigation.model";

@Component({
  selector: 'app-base-recharges',
  template: ''
})
export class BaseRechargesComponent {

  public urls: NavigationModel = {
    previousStep: '',
    nextStep: ''
  };

  public state: RechargesStateModel;

  constructor(protected router: Router) { }

  public goToNextStep(): void {
    this.router.navigate([this.urls.nextStep]);
  }

  public goToBeforeStep(): void {
    this.router.navigate([this.urls.previousStep]);
  }
}
