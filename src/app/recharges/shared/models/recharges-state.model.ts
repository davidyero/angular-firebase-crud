import {TypeRechargeModel} from "./type-recharge.model";

export interface RechargesStateModel {
  amount?: number;
  typeRecharges?: TypeRechargeModel[];
  selectedTypeRecharges?: TypeRechargeModel;
  creditCardNumber?: string;
}
