export interface NavigationModel {
  previousStep?: string;
  nextStep: string;
}
