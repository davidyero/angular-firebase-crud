import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RechargesRoutingModule} from './recharges-routing.module';
import {StepRechargeTypeComponent} from './components/step-recharge-type/step-recharge-type.component';
import {StepAmountComponent} from './components/step-amount/step-amount.component';
import {StepMethodOfPaymentComponent} from './components/step-method-of-payment/step-method-of-payment.component';
import {StepSummaryComponent} from './components/step-summary/step-summary.component';
import {StepInProcessComponent} from './components/step-in-process/step-in-process.component';
import {BaseRechargesComponent} from './shared/components/base-recharges/base-recharges.component';
import {StepperComponent} from './shared/components/stepper/stepper.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TextMaskModule} from 'angular2-text-mask';
import {CreditCardDirective} from "./shared/directives/credit-card.directive";
import {TranslocoModule} from "@ngneat/transloco";

@NgModule({
  declarations: [
    StepRechargeTypeComponent,
    StepAmountComponent,
    StepMethodOfPaymentComponent,
    StepSummaryComponent,
    StepInProcessComponent,
    BaseRechargesComponent,
    StepperComponent,
    CreditCardDirective
  ],
  imports: [
    CommonModule,
    RechargesRoutingModule,
    FormsModule,
    TextMaskModule,
    ReactiveFormsModule,
    TranslocoModule
  ]
})
export class RechargesModule {
}
