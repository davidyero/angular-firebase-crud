import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SharedConstants} from "../shared/constants/shared-constants";
import {LoginGuard} from "../shared/guards/login.guard";
import {StepAmountComponent} from "./components/step-amount/step-amount.component";
import {StepRechargeTypeComponent} from "./components/step-recharge-type/step-recharge-type.component";
import {StepMethodOfPaymentComponent} from "./components/step-method-of-payment/step-method-of-payment.component";
import {StepSummaryComponent} from "./components/step-summary/step-summary.component";
import {RechargesStateGuard} from "./guards/recharges-state.guard";
import {StepInProcessComponent} from "./components/step-in-process/step-in-process.component";

const routes: Routes = [
  {
    path: SharedConstants.ROUTES.RECHARGES,
    canActivate: [LoginGuard],
    children: [
      {
        path: SharedConstants.ROUTES.RECHARGES_FLOW.RECHARGE_TYPE,
        component: StepRechargeTypeComponent
      },
      {
        path: SharedConstants.ROUTES.RECHARGES_FLOW.AMOUNT,
        component: StepAmountComponent,
        canActivate: [RechargesStateGuard]
      },
      {
        path: SharedConstants.ROUTES.RECHARGES_FLOW.METHOD_OF_PAYMENT,
        component: StepMethodOfPaymentComponent,
        canActivate: [RechargesStateGuard]
      },
      {
        path: SharedConstants.ROUTES.RECHARGES_FLOW.SUMMARY,
        component: StepSummaryComponent,
        canActivate: [RechargesStateGuard]
      },
      {
        path: SharedConstants.ROUTES.RECHARGES_FLOW.IN_PROCESS,
        component: StepInProcessComponent,
        canActivate: [RechargesStateGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [LoginGuard],
  exports: [RouterModule]
})
export class RechargesRoutingModule {
}
