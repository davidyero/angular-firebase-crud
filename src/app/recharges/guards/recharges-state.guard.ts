import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {FacadeService} from "../shared/services/facade.service";
import {SharedConstants} from "../../shared/constants/shared-constants";

@Injectable({
  providedIn: 'root'
})
export class RechargesStateGuard implements CanActivate {

  constructor(private facadeService: FacadeService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.facadeService.getRechargesState()) {
      return true;
    } else {
      this.router.navigate([SharedConstants.ROUTES.RECHARGES, SharedConstants.ROUTES.RECHARGES_FLOW.RECHARGE_TYPE])
    }
  }

}
