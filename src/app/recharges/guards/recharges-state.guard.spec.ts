import { TestBed, async, inject } from '@angular/core/testing';

import { RechargesStateGuard } from './recharges-state.guard';

describe('RechargesStateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RechargesStateGuard]
    });
  });

  it('should ...', inject([RechargesStateGuard], (guard: RechargesStateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
