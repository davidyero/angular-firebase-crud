import { Component, OnInit } from '@angular/core';
import {SharedConstants} from "../../../shared/constants/shared-constants";
import {Router} from "@angular/router";
import {FacadeService} from "../../shared/services/facade.service";
import {BaseRechargesComponent} from "../../shared/components/base-recharges/base-recharges.component";

@Component({
  selector: 'app-step-summary',
  templateUrl: './step-summary.component.html',
  styleUrls: ['./step-summary.component.scss']
})
export class StepSummaryComponent extends BaseRechargesComponent implements OnInit{

  public currentStep = 4;
  public totalSteps = 6;
  public nameStep = 'APP_LABELS.RECHARGES.SUMMARY.NAME';
  public URLS_FLOW = SharedConstants.ROUTES.RECHARGES_FLOW;
  public urls = {
    nextStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.IN_PROCESS}`
  };

  constructor(protected router: Router, private facadeService: FacadeService) {
    super(router);
  }

  ngOnInit(): void {
    this.initializeData();
  }

  private initializeData(): void {
    this.state = this.facadeService.getRechargesState();
  }

  public redirectTo(route: string): void {
    this.router.navigate([SharedConstants.ROUTES.RECHARGES,  route]);
  }

}
