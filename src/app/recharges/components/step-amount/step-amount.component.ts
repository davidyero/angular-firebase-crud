import {Component, OnInit} from '@angular/core';
import {SharedConstants} from "../../../shared/constants/shared-constants";
import {Router} from "@angular/router";
import {FacadeService} from "../../shared/services/facade.service";
import {BaseRechargesComponent} from "../../shared/components/base-recharges/base-recharges.component";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-step-amount',
  templateUrl: './step-amount.component.html',
  styleUrls: ['./step-amount.component.scss']
})
export class StepAmountComponent extends BaseRechargesComponent implements OnInit {

  public currentStep = 2;
  public totalSteps = 5;
  public nameStep = 'APP_LABELS.RECHARGES.AMOUNT.NAME';
  public placeholder = 'APP_LABELS.RECHARGES.AMOUNT.NAME';
  public minAmount = 1;
  public maxAmount = 1000000;
  public isValid = false;
  public amountForm = new FormControl('', [Validators.min(this.minAmount), Validators.max(this.maxAmount)]);
  public urls = {
    previousStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.RECHARGE_TYPE}`,
    nextStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.METHOD_OF_PAYMENT}`
  };

  constructor(protected router: Router, private facadeService: FacadeService) {
    super(router);
  }

  ngOnInit(): void {
    this.initializeData();
  }

  public setAmount(amountInput: string): void {
    const amount = Number(amountInput);
    if(amount > this.minAmount && amount < this.maxAmount){
      this.isValid = true;
      this.updateState(SharedConstants.STATE_PARAMETERS.AMOUNT, amount);
    } else {
      this.isValid = false;
    }
  }

  private initializeData(): void {
    this.state = this.facadeService.getRechargesState();
    if (this.state && this.state.amount) {
      this.amountForm.setValue(this.state.amount);
      this.isValid = true;
    }
  }

  private updateState(param: string, value: any): void {
    this.state = {
      ...this.state,
      [param]: value
    };
    this.facadeService.setRechargesState(this.state);
  }
}
