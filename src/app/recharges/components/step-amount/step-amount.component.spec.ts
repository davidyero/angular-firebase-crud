import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepAmountComponent } from './step-amount.component';

describe('StepAmountComponent', () => {
  let component: StepAmountComponent;
  let fixture: ComponentFixture<StepAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
