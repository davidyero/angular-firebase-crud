import { Component, OnInit } from '@angular/core';
import {SharedConstants} from "../../../shared/constants/shared-constants";
import {Router} from "@angular/router";
import {FacadeService} from "../../shared/services/facade.service";
import {BaseRechargesComponent} from "../../shared/components/base-recharges/base-recharges.component";

@Component({
  selector: 'app-step-method-of-payment',
  templateUrl: './step-method-of-payment.component.html',
  styleUrls: ['./step-method-of-payment.component.scss']
})
export class StepMethodOfPaymentComponent extends BaseRechargesComponent implements OnInit {

  public currentStep = 3;
  public totalSteps = 6;
  public creditCardNumber = '';
  public nameStep = 'APP_LABELS.RECHARGES.METHOD.NAME';
  public placeholder = 'APP_LABELS.RECHARGES.METHOD.PLACEHOLDER';
  public isValid = false;
  public CREDIT_CARD_NUMBER = 19;
  public urls = {
    previousStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.AMOUNT}`,
    nextStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.SUMMARY}`
  };

  constructor(protected router: Router, private facadeService: FacadeService) {
    super(router);
  }

  ngOnInit() {
    this.initializeData();
  }

  public validateCreditCard(creditCard: string): void{
    if(creditCard.length === this.CREDIT_CARD_NUMBER) {
      this.updateState(SharedConstants.STATE_PARAMETERS.CREDIT_CARD_NUMBER, creditCard);
      this.isValid = true;
    } else {
      this.updateState(SharedConstants.STATE_PARAMETERS.CREDIT_CARD_NUMBER, '');
      this.isValid = false;
    }
  }

  private initializeData(): void {
    this.state = this.facadeService.getRechargesState();
    if(this.state && this.state.creditCardNumber) {
      this.creditCardNumber = this.state.creditCardNumber;
      this.isValid = true;
    }
  }

  private updateState(param: string, value: any): void {
    this.state = {
      ...this.state,
      [param]: value
    };
    this.facadeService.setRechargesState(this.state);
  }
}
