import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepMethodOfPaymentComponent } from './step-method-of-payment.component';

describe('StepMethodOfPaymentComponent', () => {
  let component: StepMethodOfPaymentComponent;
  let fixture: ComponentFixture<StepMethodOfPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepMethodOfPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepMethodOfPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
