import {Component, OnInit} from '@angular/core';
import {SharedConstants} from "../../../shared/constants/shared-constants";
import {Router} from "@angular/router";
import {BaseRechargesComponent} from "../../shared/components/base-recharges/base-recharges.component";
import Swal from "sweetalert2";
import {FacadeService} from "../../shared/services/facade.service";
import {AuthService} from "../../../shared/services/auth.service";
import {TranslocoService} from "@ngneat/transloco";

@Component({
  selector: 'app-step-in-process',
  templateUrl: './step-in-process.component.html',
  styleUrls: ['./step-in-process.component.scss']
})
export class StepInProcessComponent extends BaseRechargesComponent implements OnInit {

  public currentStep = 5;
  public totalSteps = 5;
  public nameStep = 'APP_LABELS.RECHARGES.IN_PROCESS.NAME';

  constructor(protected router: Router, private facadeService: FacadeService, private translocoService: TranslocoService) {
    super(router);
  }

  ngOnInit(): void {
    setTimeout(() => this.showModal(), 3000);
  }

  private showModal(): void {
    this.resetState();
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.RECHARGES.IN_PROCESS.TITLE_SWAL'),
      icon: 'success',
      showConfirmButton: false,
      timer: 1500
    }).then(() => {
      this.router.navigate([SharedConstants.ROUTES.RECHARGES, SharedConstants.ROUTES.RECHARGES_FLOW.RECHARGE_TYPE]);
    })
  }

  private resetState(): void {
    this.facadeService.setRechargesState({});
  }
}
