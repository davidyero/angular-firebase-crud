import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepRechargeTypeComponent } from './step-recharge-type.component';

describe('StepRechargeTypeComponent', () => {
  let component: StepRechargeTypeComponent;
  let fixture: ComponentFixture<StepRechargeTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepRechargeTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepRechargeTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
