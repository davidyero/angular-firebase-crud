import {Component, OnInit} from '@angular/core';
import {BaseRechargesComponent} from "../../shared/components/base-recharges/base-recharges.component";
import {Router} from "@angular/router";
import {SharedConstants} from "../../../shared/constants/shared-constants";
import {FacadeService} from "../../shared/services/facade.service";
import {TypeRechargeModel} from "../../shared/models/type-recharge.model";

@Component({
  selector: 'app-step-recharge-type',
  templateUrl: './step-recharge-type.component.html',
  styleUrls: ['./step-recharge-type.component.scss']
})
export class StepRechargeTypeComponent extends BaseRechargesComponent implements OnInit {

  public currentStep = 1;
  public totalSteps = 6;
  public nameStep = 'APP_LABELS.RECHARGES.TYPE.NAME';

  public urls = {
    previousStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.RECHARGE_TYPE}`,
    nextStep: `${SharedConstants.ROUTES.RECHARGES}/${SharedConstants.ROUTES.RECHARGES_FLOW.AMOUNT}`
  };

  constructor(protected router: Router, private facadeService: FacadeService) {
    super(router);
  }

  ngOnInit(): void {
    this.initializeData();
  }

  public selectTypeRecharge(type: TypeRechargeModel): void {
    this.updateState(SharedConstants.STATE_PARAMETERS.SELECTED_TYPE_RECHARGES, type);
    this.goToNextStep();
  };

  private initializeData(): void {
    this.state = this.facadeService.getRechargesState();
    if(!(this.state && this.state.selectedTypeRecharges)) {
      this.getTypeRecharges();
    }
  }

  private getTypeRecharges(): void {
    this.facadeService.getTypeRecharges().subscribe(
      (response: TypeRechargeModel[]) => {
        this.updateState(SharedConstants.STATE_PARAMETERS.TYPE_RECHARGES, response);
      }
    );
  }

  private updateState(param: string, value: any): void {
    this.state = {
      ...this.state,
      [param]: value
    };
    this.facadeService.setRechargesState(this.state);
  }
}
