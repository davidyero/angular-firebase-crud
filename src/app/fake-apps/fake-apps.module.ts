import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FakeAppsRoutingModule } from './fake-apps-routing.module';
import { PinterestComponent } from './shared/components/pinterest/pinterest.component';
import { InstagramComponent } from './shared/components/instagram/instagram.component';
import { DashboardAppsComponent } from './dashboard-apps/dashboard-apps.component';


@NgModule({
  declarations: [PinterestComponent, InstagramComponent, DashboardAppsComponent],
  imports: [
    CommonModule,
    FakeAppsRoutingModule
  ]
})
export class FakeAppsModule { }
