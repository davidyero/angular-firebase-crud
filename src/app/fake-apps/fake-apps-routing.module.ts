import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SharedConstants} from "../shared/constants/shared-constants";
import {InstagramComponent} from "./shared/components/instagram/instagram.component";
import {PinterestComponent} from "./shared/components/pinterest/pinterest.component";
import {DashboardAppsComponent} from "./dashboard-apps/dashboard-apps.component";

const routes: Routes = [
  {
    path: SharedConstants.ROUTES.FAKE_APPS_FLOW.MAIN,
    children: [
      {
        path: SharedConstants.ROUTES.FAKE_APPS_FLOW.DASHBOARD,
        component: DashboardAppsComponent
      },
      {
        path: SharedConstants.ROUTES.FAKE_APPS_FLOW.INSTAGRAM,
        component: InstagramComponent
      },
      {
        path: SharedConstants.ROUTES.FAKE_APPS_FLOW.PINTEREST,
        component: PinterestComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FakeAppsRoutingModule {
}
