import {Component, HostListener} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html',
  styleUrls: ['./instagram.component.scss']
})
export class InstagramComponent {

  public totalRows = 6;
  public isLoading = false;

  constructor(private router: Router) { }

  @HostListener("window:scroll", [])
  onScroll(): void {
    if ( ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) && !this.isLoading) {
      this.isLoading = true;
      setTimeout( () => {
        this.isLoading = false;
        this.totalRows += 1;
      }, 3000)
    }
  }

}
