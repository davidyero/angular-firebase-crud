import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {SharedConstants} from "../../shared/constants/shared-constants";

@Component({
  selector: 'app-dashboard-apps',
  templateUrl: './dashboard-apps.component.html',
  styleUrls: ['./dashboard-apps.component.scss']
})
export class DashboardAppsComponent {

  constructor(private router: Router) {
  }

  public redirectTo(route: string): void {
    this.router.navigate([SharedConstants.ROUTES.FAKE_APPS_FLOW.MAIN,route]);
  }

}
