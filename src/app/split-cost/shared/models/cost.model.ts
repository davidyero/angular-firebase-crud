export interface CostModel {
  quantity?: number,
  attempts?: number,
  productName?: string,
  cost?: number
}
