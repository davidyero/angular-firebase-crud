import {PersonModel} from "./person.model";
import {CostModel} from "./cost.model";

export interface TotalByPersonModel {
  person: PersonModel;
  cost?: CostModel[];
  total: number
}
