import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {PersonModel} from "../../models/person.model";

@Component({
  selector: 'app-split-cost-person',
  templateUrl: './split-cost-person.component.html',
  styleUrls: ['../../../../shared/styles/styles.scss']
})
export class SplitCostPersonComponent implements OnChanges{

  @Input() numberPerson: number;
  @Output() personListEvent: EventEmitter<PersonModel[]>= new EventEmitter();
  @Output() beforeStep: EventEmitter<boolean> = new EventEmitter();
  @Output() nextStep: EventEmitter<boolean> = new EventEmitter();

  public personList: PersonModel[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    this.createPersonList();
  }

  public goToNextStep(): void {
    this.personListEvent.emit(this.personList);
    this.nextStep.emit(true);
  }

  public goToBeforeStep(): void {
    this.beforeStep.emit(true);
  }

  public validateForm(): boolean {
    let validate = true;
    this.personList.map(person => {
      return validate = validate && (person.person !== '');
    });
    return validate;
  }

  private createPersonList(): void {
    this.personList = [];
    for(let i = 0; i < this.numberPerson; i++) {
      this.personList.push({person: ''});
    }
  }
}
