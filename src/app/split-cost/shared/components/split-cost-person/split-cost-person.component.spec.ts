import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitCostPersonComponent } from './split-cost-person.component';

describe('SplitCostPersonComponent', () => {
  let component: SplitCostPersonComponent;
  let fixture: ComponentFixture<SplitCostPersonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitCostPersonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitCostPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
