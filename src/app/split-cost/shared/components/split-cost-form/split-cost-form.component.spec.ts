import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitCostFormComponent } from './split-cost-form.component';

describe('SplitCostFormComponent', () => {
  let component: SplitCostFormComponent;
  let fixture: ComponentFixture<SplitCostFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitCostFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitCostFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
