import {Component, EventEmitter, Output} from '@angular/core';
import {CostModel} from "../../models/cost.model";

@Component({
  selector: 'app-split-cost-form',
  templateUrl: './split-cost-form.component.html',
  styleUrls: ['../../../../shared/styles/styles.scss']
})
export class SplitCostFormComponent {

  @Output() totalListEvent: EventEmitter<CostModel[]> = new EventEmitter();
  @Output() tipEvent: EventEmitter<number> = new EventEmitter();
  @Output() numberPersonEvent: EventEmitter<number> = new EventEmitter();
  @Output() nextStep: EventEmitter<boolean> = new EventEmitter();

  public initialTableRow: CostModel = {};
  public numberRows: CostModel[] = [{...this.initialTableRow}];
  public tip;
  public minRows = 1;
  public maxRows = 10;
  public numberPerson;
  public total = 0;

  public addRow(): void {
    if (this.numberRows.length !== this.maxRows) {
      this.numberRows = [...this.numberRows, {...this.initialTableRow}];
    }
  }

  public removeRow(index: number): void {
    if (this.numberRows.length !== this.minRows) {
      this.numberRows = this.numberRows.filter(function (value, i) {
        return i !== index;
      });
    }
  }

  public validateForm(): boolean {
    return Boolean(this.numberPerson) && Boolean(this.total);
  }

  public calculateTotal(): void {
    this.total = this.tip ? this.tip : 0;

    this.numberRows.map(row => {
      this.total += (row.cost * row.quantity) ? (row.cost * row.quantity) : 0;
    });
  }

  public goToNextStep(): void {
    this.totalListEvent.emit(this.numberRows);
    this.tipEvent.emit(this.tip);
    this.numberPersonEvent.emit(this.numberPerson);
    this.nextStep.emit(true);
  }
}
