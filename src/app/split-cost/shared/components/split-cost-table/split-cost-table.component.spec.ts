import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitCostTableComponent } from './split-cost-table.component';

describe('SplitCostTableComponent', () => {
  let component: SplitCostTableComponent;
  let fixture: ComponentFixture<SplitCostTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitCostTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitCostTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
