import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {CostModel} from "../../models/cost.model";
import {PersonModel} from "../../models/person.model";
import {TotalByPersonModel} from "../../models/total-by-person.model";

@Component({
  selector: 'app-split-cost-table',
  templateUrl: './split-cost-table.component.html',
  styleUrls: ['../../../../shared/styles/styles.scss']
})
export class SplitCostTableComponent implements OnChanges{

  @Input() personList: PersonModel[];
  @Input() costList: CostModel[];
  @Input() tip: number;
  @Output() beforeStep: EventEmitter<boolean> = new EventEmitter();
  @Output() reset: EventEmitter<boolean> = new EventEmitter();

  public totalCost: TotalByPersonModel[];
  public total;
  public totalAttempts;
  public enableAttempts;
  public couplesList;

  ngOnChanges(): void {
    this.initializeData();
  }

  public selectedCell(cost: CostModel, person: PersonModel, i: number, j: number, k: number): void {
    if((document.querySelector('.back' + i.toString() + j.toString() + k.toString()) as HTMLElement).style.background) {
      (document.querySelector('.back' + i.toString() + j.toString() + k.toString()) as HTMLElement).style.background = '';
      this.addOrSubtract(false, cost, person);
    } else {
      if(cost.attempts > 0){
        (document.querySelector('.back' + i.toString() + j.toString() + k.toString()) as HTMLElement).style.background = 'red';
        this.addOrSubtract(true, cost, person);
      }
    }
  }

  public goToNextStep(): void {
    this.reset.emit(true);
  }

  public goToBeforeStep(): void {
    this.beforeStep.emit(true);
  }

  private initializeData(): void {
    const totalTip = this.tip/this.personList.length;
    this.totalCost = [];
    this.enableAttempts = 0;
    this.totalAttempts = 1;
    this.personList.map(person => {
      this.totalCost.push({person, total: totalTip})
    });
    this.costList.map(cost => {
      this.totalAttempts += cost.attempts;
      this.enableAttempts += cost.attempts;
    });
    this.total = this.tip;
    this.createCouplesList();
  }

  private addOrSubtract(add: boolean, cost: CostModel, person: PersonModel): void {
    this.totalCost.map(productSelected => {
      if(productSelected.person === person){
        if(add) {
          productSelected.total = productSelected.total + cost.cost;
          this.total = this.total + cost.cost;
          this.enableAttempts -= 1;
          cost.attempts -= 1;
        } else {
          productSelected.total = productSelected.total - cost.cost;
          this.total = this.total - cost.cost;
          this.enableAttempts += 1;
          cost.attempts += 1;
        }
      }
    });
  }

  private createCouplesList(): void {
    let couple = [];
    this.couplesList = [];
    this.totalCost.map( (person, index) => {
      if(couple.length === 2){
        this.couplesList.push(couple);
        couple = [];
      }
      couple.push(person);
      if(index === (this.totalCost.length - 1)){
        this.couplesList.push(couple);
      }
    });
  }
}
