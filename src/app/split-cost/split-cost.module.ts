import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SplitCostRoutingModule } from './split-cost-routing.module';
import { SplitCostComponent } from './split-cost/split-cost.component';
import { SplitCostFormComponent } from './shared/components/split-cost-form/split-cost-form.component';
import { FormsModule } from "@angular/forms";
import { SplitCostTableComponent } from './shared/components/split-cost-table/split-cost-table.component';
import { SplitCostPersonComponent } from './shared/components/split-cost-person/split-cost-person.component';
import { SortablejsModule } from "ngx-sortablejs";
import { TranslocoModule } from "@ngneat/transloco";


@NgModule({
  declarations: [
    SplitCostComponent,
    SplitCostFormComponent,
    SplitCostTableComponent,
    SplitCostPersonComponent
  ],
  imports: [
    CommonModule,
    SplitCostRoutingModule,
    FormsModule,
    SortablejsModule,
    SortablejsModule,
    TranslocoModule
  ]
})
export class SplitCostModule { }
