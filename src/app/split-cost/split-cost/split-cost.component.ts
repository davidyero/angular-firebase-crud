import {Component} from '@angular/core';
import {CostModel} from "../shared/models/cost.model";
import {PersonModel} from "../shared/models/person.model";

@Component({
  selector: 'app-split-cost',
  templateUrl: './split-cost.component.html',
  styleUrls: ['../../shared/styles/styles.scss']
})
export class SplitCostComponent {

  public costList: CostModel[];
  public personList: PersonModel[];
  public tip: number;
  public numberPerson: number;
  public showProductsForm = true;
  public showPersonForm = false;
  public showTableForm = false;

  public checkFormCost(costList: CostModel[]): void {
    costList.map(cost => {
      cost.attempts = cost.quantity;
    });
    this.costList = costList;
  }

  public checkFormTip(tip: number): void {
    this.tip = tip;
  }

  public checkFormNumberPerson(numberPerson: number): void {
    this.numberPerson = numberPerson;
  }

  public checkFormPerson(personList: PersonModel[]): void {
    this.personList = personList;
  }

  public productFormEvent(): void {
    this.showProductsForm = true;
    this.showPersonForm = false;
    this.showTableForm = false;
  }

  public personFormEvent(): void {
    this.showProductsForm = false;
    this.showPersonForm = true;
    this.showTableForm = false;
  }

  public tableFormEvent(): void {
    this.showProductsForm = false;
    this.showPersonForm = false;
    this.showTableForm = true;
  }
}
