import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitCostComponent } from './split-cost.component';

describe('SplitCostComponent', () => {
  let component: SplitCostComponent;
  let fixture: ComponentFixture<SplitCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
