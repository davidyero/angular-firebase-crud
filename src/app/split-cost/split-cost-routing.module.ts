import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SplitCostComponent} from "./split-cost/split-cost.component";
import {SharedConstants} from "../shared/constants/shared-constants";


const routes: Routes = [
  {
    path: SharedConstants.ROUTES.SPLIT_COST,
    component: SplitCostComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SplitCostRoutingModule { }
