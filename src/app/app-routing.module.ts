import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SharedConstants} from "./shared/constants/shared-constants";
import {DashboardComponent} from "./dashboard/dashboard.component";


const routes: Routes = [
  {
    path: SharedConstants.ROUTES.MAIN,
    component: DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
