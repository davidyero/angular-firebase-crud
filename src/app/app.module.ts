import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CrudModule} from "./crud/crud.module";
import {DashboardComponent} from './dashboard/dashboard.component';
import {NavbarComponent} from './shared/components/navbar/navbar.component';
import {SplitCostModule} from "./split-cost/split-cost.module";
import {SortablejsModule} from "ngx-sortablejs";
import {registerLocaleData} from "@angular/common";
import localeCO from '@angular/common/locales/es-CO';
import localeUS from '@angular/common/locales/en-US-POSIX';
import {HttpClientModule} from '@angular/common/http';
import {TranslocoRootModule} from './transloco-root.module';
import {RechargesModule} from "./recharges/recharges.module";
import {NotFoundComponent} from './shared/components/not-found/not-found.component';
import {FakeAppsModule} from "./fake-apps/fake-apps.module";

registerLocaleData(localeCO, 'en-CO');
registerLocaleData(localeUS, 'en-US');

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NavbarComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CrudModule,
    SplitCostModule,
    SortablejsModule.forRoot({}),
    HttpClientModule,
    TranslocoRootModule,
    RechargesModule,
    FakeAppsModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
