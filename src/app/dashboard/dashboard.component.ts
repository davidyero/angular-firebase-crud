import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import * as moment from 'moment';
import {SharedConstants} from "../shared/constants/shared-constants";
import {LoginService} from "../shared/services/login.service";
import {AuthService} from "../shared/services/auth.service";
import * as configcat from "configcat-js";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../shared/styles/styles.scss']
})
export class DashboardComponent implements OnInit{

  public URLS = SharedConstants.ROUTES;
  public showCRUD = true;
  public showSplitCost = true;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private authService: AuthService) {
  }

  ngOnInit(): void {
    // this.loadFlags();
  }

  navigateTo(route: string) {
    this.router.navigate([route])
  }

  getDate(createDate: string): string {
    return moment(createDate).fromNow();
  }

  loginValidate(route: string): void {
    if (this.authService.isLoggedIn()) {
      this.navigateTo(route);
    } else {
      this.loginService.showLoginModal();
    }
  }

  loadFlags() {
    let configCatClient = configcat.createClient(environment.configcat);

    configCatClient.getValue("showcrud",  undefined, value => {
      this.showCRUD = value;
    });

    configCatClient.getValue("showsplitcost",  undefined, value => {
      this.showSplitCost = value;
    });
  }
}
