import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {AuthService} from "../services/auth.service";
import {SharedConstants} from "../constants/shared-constants";

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if(!this.authService.isLoggedIn()){
      this.router.navigate([SharedConstants.ROUTES.MAIN]);
    }
    return this.authService.isLoggedIn();
  }

}
