import {Injectable} from '@angular/core';
import {Observable, of} from "rxjs";
import {User} from "../models/user";
import {delay} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private USER_TEST = 'David';
  private PASSWORD_TEST = '12345678';

  public validateUserInformation(user: User): Observable<boolean> {
    return of(user.user === this.USER_TEST && user.password === this.PASSWORD_TEST).pipe(delay(3000));
  }
}
