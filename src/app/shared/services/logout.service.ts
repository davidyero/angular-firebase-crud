import { Injectable } from '@angular/core';
import {AuthService} from "./auth.service";
import Swal from "sweetalert2";
import {TranslocoService} from "@ngneat/transloco";

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private authService: AuthService, private translocoService: TranslocoService) { }

  logout(): void {
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGOUT.TITLE'),
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGOUT.BUTTONS.CANCEL'),
      confirmButtonColor: '#d33',
      confirmButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGOUT.BUTTONS.LOGOUT')
    }).then((result) => {
      if (result.value) {
        this.authService.logout();
      }
    })
  }

}
