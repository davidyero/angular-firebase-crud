import {Injectable} from '@angular/core';
import Swal from "sweetalert2";
import {AppService} from "./app.service";
import {AuthService} from "./auth.service";
import {TranslocoService} from "@ngneat/transloco";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private appService: AppService,
    private authService: AuthService,
    private translocoService: TranslocoService) {
  }

  showLoginModal(): void {
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.LOGIN'),
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.CANCEL'),
      cancelButtonColor: '#d33',
      confirmButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.LOGIN')
    }).then((result) => {
      if (result.value) {
        this.showUsernameModal()
      }
    })
  }

  showUsernameModal(username = ''): void {
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.USER'),
      input: 'text',
      inputValue: username,
      inputPlaceholder: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.USER'),
      icon: 'question',
      confirmButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.NEXT'),
      showCloseButton: true,
      preConfirm: (inputValue) => {
        if (inputValue !== '') {
          this.showPasswordModal(inputValue);
        } else {
          Swal.showValidationMessage(this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.ERROR_MESSAGES.USER_ERROR'))
        }
      }
    })
  }

  showPasswordModal(user: string): void {
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.PASSWORD'),
      input: 'password',
      inputPlaceholder: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.PASSWORD'),
      icon: 'question',
      showCancelButton: true,
      cancelButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.BACK'),
      confirmButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.LOGIN'),
      showLoaderOnConfirm: true,
      showCloseButton: true,
      reverseButtons: true,
      preConfirm: async (password) => {
        if (password !== '') {
          return await this.userValidation(user, password)
            .then((name) => {
              if (Boolean(name)) {
                this.successLogin(user, password);
              } else {
                this.errorLogin();
              }
            })
        } else {
          Swal.showValidationMessage(this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.ERROR_MESSAGES.PASSWORD_ERROR'))
        }
      }
    }).then((result) => {
      if (result.dismiss.toString() === 'cancel') {
        this.showUsernameModal(user);
      }
    })
  }

  async userValidation(user: string, password: string): Promise<boolean> {
    return await new Promise(((resolve, reject) => {
      this.appService.validateUserInformation({user, password})
        .subscribe((response: boolean) => {
            resolve(response);
          },
          () => {
            reject(false);
          });
    }));
  }

  successLogin(user: string, password: string): void {
    this.authService.signIn({user, password});
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.WELCOME'),
      icon: 'success',
      showConfirmButton: false,
      timer: 1500
    })
  }

  errorLogin(): void {
    Swal.fire({
      title: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.ERROR_MESSAGES.LOGIN_ERROR'),
      icon: 'error',
      showCancelButton: true,
      cancelButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.CANCEL'),
      cancelButtonColor: '#d33',
      confirmButtonText: this.translocoService.translate('APP_LABELS.AUTHORIZATION.LOGIN.BUTTONS.RETRY'),
    }).then((result) => {
      if (result.value) {
        this.showUsernameModal();
      }
    })
  }
}
