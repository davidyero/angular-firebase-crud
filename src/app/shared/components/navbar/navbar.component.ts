import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {SharedConstants} from "../../constants/shared-constants";
import {LoginService} from "../../services/login.service";
import {AuthService} from "../../services/auth.service";
import {LogoutService} from "../../services/logout.service";
import {TranslocoService} from "@ngneat/transloco";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  languages = [
    { flag: '🇨🇴 ', label: 'LANGUAGES.ES', value: 'es' },
    { flag: '🇺🇸 ', label: 'LANGUAGES.EN', value: 'en' },
  ];

  URLS = SharedConstants.ROUTES;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private authService: AuthService,
    private logoutService: LogoutService,
    private translocoService: TranslocoService) {
  }

  navigateTo(route: string) {
    this.router.navigate([route])
  }

  loginValidate(route: string): void {
    if (this.authService.isLoggedIn()) {
      this.navigateTo(route);
    } else {
      this.loginService.showLoginModal();
    }
  }

  hasRegistered(): boolean {
    return this.authService.isLoggedIn();
  }

  login(): void {
    this.loginService.showUsernameModal()
  }

  logout(): void {
    this.logoutService.logout();
  }

  updateLocale(lang: string) {
    this.translocoService.setActiveLang(lang);
  }
}
