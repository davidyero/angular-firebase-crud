export class SharedConstants {
  static ROUTES = {
    MAIN: '',
    CRUD: 'realizar-pedido',
    SPLIT_COST: 'repartir-costo',
    RECHARGES: 'recargas',
    RECHARGES_FLOW: {
      RECHARGE_TYPE: 'tipo-de-recarga',
      AMOUNT: 'monto',
      METHOD_OF_PAYMENT: 'forma-de-pago',
      SUMMARY: 'resumen',
      IN_PROCESS: 'pago-en-proceso',
    },
    FAKE_APPS_FLOW: {
      MAIN: 'aplicaciones',
      DASHBOARD: 'dashboard',
      INSTAGRAM: 'instagram',
      PINTEREST: 'pinterest'
    }
  };

  static STATE_PARAMETERS = {
    TYPE_RECHARGES: 'typeRecharges',
    AMOUNT: 'amount',
    SELECTED_TYPE_RECHARGES: 'selectedTypeRecharges',
    CREDIT_CARD_NUMBER: 'creditCardNumber',
  }
}
